from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Animation, Image


@login_required
def index(request):
    print('*'*30)
    print(request.user)

    if request.method == 'POST':
        anim = Animation.objects.create(user=request.user, name=request.POST['name'])
        
        for img in request.FILES.getlist('imgs'):
            Image.objects.create(animation=anim, image=img)

    anims = Animation.objects.filter(user=request.user)
    context = {
            'anims': anims
            }
    return render(request, 'mkgif/index.html', context)

@login_required
def details(request, pk):
    anim = get_object_or_404(Animation, pk=pk)
    images = Image.objects.filter(animation=pk)
    context = {
            'anim': anim,
            'images': images
            }
    return render(request, 'mkgif/details.html', context)
