from django.contrib import admin
from .models import Animation, Image

admin.site.register(Animation)
admin.site.register(Image)
