# Generated by Django 4.1.2 on 2022-10-23 20:40

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import mkgif.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Animation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to=mkgif.models.Image.image_path)),
                ('animation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mkgif.animation')),
            ],
        ),
    ]
